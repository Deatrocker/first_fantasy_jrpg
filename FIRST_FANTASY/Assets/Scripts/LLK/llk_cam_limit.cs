﻿using UnityEngine;

public class llk_cam_limit : MonoBehaviour {
    [SerializeField] bool lim_x, lim_y, lim_z;
    [SerializeField] float x_lim_min, y_lim_min;
    [SerializeField] float x_lim_max, y_lim_max;
    [SerializeField] float z_lim_max, z_lim_min;
    [SerializeField] Vector3 offset;
    GameObject player; // player GO

    private void Start() {
        player = GameObject.FindWithTag("Player"); // Find our player by tag
    }

    private void LateUpdate() {
        if(lim_x) {
            var x_clamped = Mathf.Clamp(player.transform.position.x, x_lim_min, x_lim_max);
            transform.position = new Vector3(x_clamped + offset.x, transform.position.y, transform.position.z);
        }
        if(lim_y) {
            var y_clamped = Mathf.Clamp(player.transform.position.y, y_lim_min, y_lim_max);
            transform.position = new Vector3(transform.position.x, y_clamped + offset.y, transform.position.z);
        }
        if(lim_z) {
            var z_clamped = Mathf.Clamp(player.transform.position.z, z_lim_min, z_lim_max);
            transform.position = new Vector3(transform.position.x, transform.position.y, z_clamped + offset.z);
        }
    }

}
