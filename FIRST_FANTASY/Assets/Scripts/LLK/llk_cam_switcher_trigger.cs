﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class llk_cam_switcher_trigger : MonoBehaviour {
    [SerializeField] GameObject Camera1, Camera2;

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player") {
            Camera1.SetActive(false);
            Camera2.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.tag == "Player") {
            Camera2.SetActive(false);
            Camera1.SetActive(true);
        }
    }
}
