﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Developer: Dmitry Deatrocker Raskalov https://vk.com/deatrocker and https://connect.unity.com/u/59e1bcd0880c64001985c55d (2018)
/// All game data manager
/// </summary>

static public class DATA_MANAGER { // Реализация скриптового синглтона
    #region VARIABLES_REGION
    // Globals
    public const float walk_speed = 1.2f;
    // Slots block
    [SerializeField] const uint slots_amount = 3; // how much slots do we have

    // Position data block
    [SerializeField] static Vector3 s_next_scene_start_position; // Stores next position between scenes
    [SerializeField] static Vector3 s_next_scene_start_rotation; // Stores next rotation between scenes

    static AsyncOperation ao;
    #endregion

    #region PUBLIC_INTERFACE
    /// <summary>
    /// If slot not sended, method returns true if at least one slot have saves
    /// If slot is sended, return true if target slot got save data
    /// Else returns false
    /// </summary>
    /// <param name="_slot"></param>
    /// <returns></returns>
    static public bool Has_Save(int _slot = -1) { // Check if we have save
        if(_slot < 0) if(PlayerPrefs.GetInt("GotSave") == 1) return true;
        if(Get_Slot_Exist(_slot) == true) return true; // if we want to know existence in target slot
        return false;
    }

    /// <summary>
    /// Saves whole game to target slot
    /// </summary>
    /// <param name="_slot"></param>
    static public void Save_To_Slot(int _slot) {
        Set_Current_Slot(_slot); // Set slot for working with (also set it existance)
        Set_Save_Scene_Index(); // Save scene index
        Set_Save_Point(GameObject.FindWithTag("Player").transform.position); // Save player position
        PlayerPrefs.SetInt("GotSave", 1);
        Debug.Log("Save completed");
    }

    /// <summary>
    /// Loads whole game from target slot
    /// </summary>
    /// <param name="_slot"></param>
    static public IEnumerator Load_From_Slot(int _slot) {
        Set_Current_Slot(_slot, true); // Set slot for working with
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        ao = SceneManager.LoadSceneAsync(Get_Saved_Scene_Index());
        while(!ao.isDone) yield return null;
        
    }

    /// <summary>
    /// Set next scene position player should load from
    /// </summary>
    /// <param name="_next_pos"></param>
    static public void Set_Net_Scene_Location(Vector3 _next_pos, Vector3 _rot) {
        s_next_scene_start_position = _next_pos;
        s_next_scene_start_rotation = _rot;
    }

    /// <summary>
    /// Get next scene position
    /// </summary>
    /// <returns></returns>
    static public Vector3 Get_Next_Scene_Position() {
        return s_next_scene_start_position;
    }

    /// <summary>
    /// Get next scene rotation
    /// </summary>
    /// <returns></returns>
    static public Vector3 Get_Next_Scene_Rotation() {
        return s_next_scene_start_rotation;
    }

    /// <summary>
    /// Get flag status - is this is loaded game or its a new game
    /// </summary>
    /// <returns></returns>
    static public bool Get_Loaded_Status() {
        if(PlayerPrefs.GetInt("loaded_status") == 0) return false;
        return true;
    }

    /// <summary>
    /// Set flag - is this is loaded game or its a new game
    /// </summary>
    /// <param name="_status"></param>
    static public void Set_Loaded_Status(bool _status) {
       if(_status == true) PlayerPrefs.SetInt("loaded_status", 1);
       if(_status == false) PlayerPrefs.SetInt("loaded_status", 0);
    }

    #endregion

    #region PRIVATE_METHODS
    /// <summary>
    /// Метод устанавливает положение ГГ после загрузки сцены, и отписывает свою прослушку.
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    static void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) {
        Set_Loaded_Status(true);
        GameObject.FindWithTag("Player").transform.position = Get_Save_Point(); // Устанавливаем позицию ГГ
        SceneManager.sceneLoaded -= OnLevelFinishedLoading; // Отписываемся от прослушки
        Debug.Log("Loading complited");
    }
    /// <summary>
    /// Set current slot for working with. flag _load measns should we mark slot as existed
    /// </summary>
    /// <param name="_slot"></param>
    static void Set_Current_Slot(int _slot, bool _load = false) {
        PlayerPrefs.SetString("current_slot", _slot.ToString());
        if(!_load) Set_Slot_Exist(true);
    }

    /// <summary>
    /// Set slot existence status
    /// </summary>
    /// <param name="_status"></param>
    static void Set_Slot_Exist(bool _status) {
        if(_status == true) PlayerPrefs.SetInt("slot_exist" + Get_Current_Slot(), 1);
        if(_status == false) PlayerPrefs.SetInt("slot_exist" + Get_Current_Slot(), 0);
    }

    static bool Get_Slot_Exist(int _slot) {
        if(PlayerPrefs.GetInt("slot_exist" + _slot) == 0) return false;
        return true;
    }

    static string Get_Current_Slot() {
        return PlayerPrefs.GetString("current_slot");
    }

    /// <summary>
    /// Set current scene
    /// </summary>
    static void Set_Save_Scene_Index() {
        PlayerPrefs.SetInt("s_scene_index" + Get_Current_Slot(), SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Get saved scene index
    /// </summary>
    /// <returns></returns>
    static int Get_Saved_Scene_Index() {
        return PlayerPrefs.GetInt("s_scene_index" + Get_Current_Slot());
    }

    /// <summary>
    /// Set save point
    /// </summary>
    /// <param name="_new_rp"></param>
    static void Set_Save_Point(Vector3 _new_rp) {
        PlayerPrefs.SetFloat("s_position_point" + Get_Current_Slot() + "x", _new_rp.x);
        PlayerPrefs.SetFloat("s_position_point" + Get_Current_Slot() + "y", _new_rp.y);
        PlayerPrefs.SetFloat("s_position_point" + Get_Current_Slot() + "z", _new_rp.z);
    }

    /// <summary>
    /// Get saved save point position
    /// </summary>
    /// <returns></returns>
    static Vector3 Get_Save_Point() {
        Vector3 _loaded_pos;
        _loaded_pos.x = PlayerPrefs.GetFloat("s_position_point" + Get_Current_Slot() + "x");
        _loaded_pos.y = PlayerPrefs.GetFloat("s_position_point" + Get_Current_Slot() + "y");
        _loaded_pos.z = PlayerPrefs.GetFloat("s_position_point" + Get_Current_Slot() + "z");
        return _loaded_pos;
    }

    #endregion

}
