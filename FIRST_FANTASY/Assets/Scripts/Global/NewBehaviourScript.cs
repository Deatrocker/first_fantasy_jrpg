﻿using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {
    public int used_slot;

    void Update() {
        if(Input.GetKeyDown(KeyCode.H)) {
            DATA_MANAGER.Save_To_Slot(0);
        }
        if(Input.GetKeyDown(KeyCode.L)) {
           StartCoroutine(DATA_MANAGER.Load_From_Slot(0));
        }
        if(Input.GetKeyDown(KeyCode.P)) {
            PlayerPrefs.DeleteAll();
        }
    }


}
