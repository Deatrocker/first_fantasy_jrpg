﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
/// <summary>
/// Developer: Dmitry Deatrocker Raskalov https://vk.com/deatrocker and https://connect.unity.com/u/59e1bcd0880c64001985c55d (2018)
/// Script controlls main menu
/// </summary>

public class Main_Menu_Controller : MonoBehaviour {
    [SerializeField] GameObject selected_GO; // First menu options to show
    [SerializeField] int moovie_scene; // Movie scene buildindex
    [SerializeField] List<GameObject> pages; // List of all menu pages to manipulate them and have access to
    bool selected;

    private void OnEnable() {
        EventSystem.current.firstSelectedGameObject = selected_GO; // Select firts menu option
        EventSystem.current.SetSelectedGameObject(selected_GO, new BaseEventData(EventSystem.current)); // Set it selected
    }

    public void Menu_Action(int action) {
        switch(action) { // Menu state machine controller
            case 0: { // New Game Start
                SceneManager.LoadScene(moovie_scene); // Just start new game (play opening movie)
                break;
            }
            case 1: { // Load Game Page Open
                if(!DATA_MANAGER.Has_Save()) StartCoroutine(Show_Load_Error()); // If we have no saves at all
                else { // If we got saves
                    pages[0].gameObject.SetActive(false); // Disable main page
                    pages[1].gameObject.SetActive(true); // Activate load game page
                }
                return;
            }
            case 2: { // If we select slot to load from
                StartCoroutine(DATA_MANAGER.Load_From_Slot(0)); // Load game from selected slot
                break;
            }

        }
    }

    IEnumerator Show_Load_Error() { // Error window if no saves founded
        pages[0].gameObject.SetActive(false); // Set main page off
        pages[2].gameObject.SetActive(true); // Activate error message
        yield return new WaitForSeconds(3f); // Wait
        pages[2].gameObject.SetActive(false); // Disable error message
        pages[0].gameObject.SetActive(true); // Activate main page
    }

}
