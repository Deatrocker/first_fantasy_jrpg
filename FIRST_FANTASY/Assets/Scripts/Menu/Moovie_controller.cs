﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Developer: Dmitry Deatrocker Raskalov https://vk.com/deatrocker (2018)
/// Script blank in purposes of play opening movie
/// </summary>
/// 
public class Moovie_controller : MonoBehaviour {
    [SerializeField] int level_to_load; // Which level load after opening movie
    [SerializeField] Vector3 next_position; // Start character position in next scene
    [SerializeField] Vector3 next_rotation; // Rotation in next scene

    void Start () {
        StartCoroutine("wait_moovie"); // Just play movie
	}
	
    IEnumerator wait_moovie() {
        // play movie
        DATA_MANAGER.Set_Net_Scene_Location(next_position, next_rotation); // Set character next position and rotation
        yield return new WaitForSeconds(2f); // Blank delay (in future will be delayed till movie ends)
        SceneManager.LoadScene(level_to_load); // Load scene
    }
}
