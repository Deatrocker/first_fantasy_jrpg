﻿using System.Collections;
using UnityEngine;
/// <summary>
/// Developer: Dmitry Deatrocker Raskalov https://vk.com/deatrocker and https://connect.unity.com/u/59e1bcd0880c64001985c55d (2018)
/// Maybe this script must be deleted and remaded at all
/// </summary>

public class UI_element_scaling : MonoBehaviour {
    [SerializeField] Vector3 start_scale;
    [SerializeField] Vector3 target_scale;
    [SerializeField] float scale_speed = 1f;
    [SerializeField] bool auto_scale = true;
    [SerializeField] bool auto_hide = true;
    [SerializeField] float auto_hide_delay = 1f;

    public bool Auto_scale {
        get {
            return auto_scale;
        }

        set {
            auto_scale = value;
        }
    }

    public bool Auto_hide {
        get {
            return auto_hide;
        }

        set {
            auto_hide = value;
        }
    }
    
    private void OnEnable() {
        //if(auto_scale) StartCoroutine(UI_scale_up(GetComponent<RectTransform>(), start_scale, target_scale, scale_speed));
        if(auto_hide) StartCoroutine(UI_scale_down(GetComponent<RectTransform>(), start_scale, target_scale, scale_speed));
    }
    /*
    public IEnumerator UI_scale_up(Transform _target_ui, Vector3 _start_scale, Vector3 _target_scale, float _speed) {
        float currentTime = 0.0f;
        do {
            _target_ui.GetComponent<RectTransform>().localScale = Vector3.Lerp(_start_scale, _target_scale, currentTime / _speed);
            currentTime += Time.deltaTime;
            yield return null;
        } while(currentTime <= _speed);
        if(auto_hide) StartCoroutine(UI_scale_down(GetComponent<RectTransform>(), start_scale, target_scale, scale_speed));
    }
    */
    public IEnumerator UI_scale_down(Transform _target_ui, Vector3 _start_scale, Vector3 _target_scale, float _speed) {
        if(auto_hide) yield return new WaitForSecondsRealtime(auto_hide_delay);
        float currentTime = 0.0f;
        do {
            _target_ui.GetComponent<RectTransform>().localScale = Vector3.Lerp(_target_scale, _start_scale, currentTime / _speed);
            currentTime += Time.deltaTime;
            yield return null;
        } while(currentTime <= _speed);
        gameObject.SetActive(false);
    }
    
}
