﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class action_class {
    [SerializeField] globals.NPC_ACTION_TYPE action;
    [SerializeField] bool wait_finish;
    [SerializeField] GameObject actor;
    [SerializeField] Transform target_position;
    [SerializeField] byte anim_id;
    [SerializeField] float action_speed;
    [SerializeField] float delay;

    public globals.NPC_ACTION_TYPE Action {
        get {
            return action;
        }

        set {
            action = value;
        }
    }

    public bool Wait_finish {
        get {
            return wait_finish;
        }

        set {
            wait_finish = value;
        }
    }

    public GameObject Actor {
        get {
            return actor;
        }

        set {
            actor = value;
        }
    }

    public byte Anim_id {
        get {
            return anim_id;
        }

        set {
            anim_id = value;
        }
    }

    public float Action_speed {
        get {
            return action_speed;
        }

        set {
            action_speed = value;
        }
    }

    public Transform Target_position {
        get {
            return target_position;
        }

        set {
            target_position = value;
        }
    }

    public float Delay {
        get {
            return delay;
        }

        set {
            delay = value;
        }
    }
}
