﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class action_line : MonoBehaviour {
    [SerializeField] bool auto_execute = false;
    [SerializeField] List<action_class> actions;
    [SerializeField] short current_action_num;
    bool exec;

    void Start() {
        if(auto_execute) StartCoroutine(Execute_Action_Line());
    }

    public IEnumerator Execute_Action_Line() {
        foreach(action_class act in actions) {

            switch(act.Action) {
                case globals.NPC_ACTION_TYPE.WALK_F: {
                    yield return StartCoroutine(act.Actor.GetComponent<NPC_class>().WALK_F(act.Target_position.position));
                    break;
                }
                case globals.NPC_ACTION_TYPE.IDLE: {
                    yield return StartCoroutine(act.Actor.GetComponent<NPC_class>().IDLE(act.Delay));
                    break;
                }
            }

        }
        yield return null;
    }



}
