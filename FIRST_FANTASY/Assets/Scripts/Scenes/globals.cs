﻿using UnityEngine;
/// <summary>
/// Developer: Dmitry Deatrocker Raskalov https://vk.com/deatrocker and https://connect.unity.com/u/59e1bcd0880c64001985c55d (2018)
/// Here is global enumerators and statics
/// </summary>


static public class globals {
    
    public enum NPC_ACTION_TYPE { // This enum used for every action in scene
        IDLE, // Idle =)
        WALK_F, // For walking
        RUN_F, // For running
        ROTATE, // For rotating
        SETCONTROLL, // For manipulation of player chatacter controll
        ANIME, // For playing animation
    }
}
