﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_class : MonoBehaviour {

    public IEnumerator WALK_F(Vector3 _new_pos) {
        while(Vector3.Distance(transform.position, _new_pos) > 0.2f) {
            Vector3 new_rotation = new Vector3(_new_pos.x - transform.position.x, 0.0f, _new_pos.z - transform.position.z);
            if(!transform.GetComponent<Animation>().IsPlaying("Walk")) transform.GetComponent<Animation>().CrossFade("Walk", 0.15f);
            transform.position = Vector3.MoveTowards(transform.position, _new_pos, DATA_MANAGER.walk_speed * Time.deltaTime);
            transform.transform.rotation = Quaternion.LookRotation(new_rotation);
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator IDLE(float delay = 0.0f) {
        GetComponent<Animation>().CrossFade("Idle", 0.2f);
        if(!Mathf.Approximately(delay, 0f)) yield return new WaitForSeconds(delay);
        yield return null;
    }
}
