﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class gizmo : MonoBehaviour {
    enum GIZMO_TYPE {Cube, Sphere };
    [SerializeField] GIZMO_TYPE gizmo_type;
    [SerializeField] Color gizmo_color;
    [SerializeField] float gizmo_size;

    void OnDrawGizmosSelected() {
        Gizmos.color = gizmo_color;
        if(gizmo_type == GIZMO_TYPE.Cube) {
            Gizmos.DrawWireCube(transform.position, new Vector3(gizmo_size, gizmo_size, gizmo_size));
        }
        if(gizmo_type == GIZMO_TYPE.Sphere) {
            Gizmos.DrawWireSphere(transform.position, gizmo_size);
        }

    }


}
