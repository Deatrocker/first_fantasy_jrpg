﻿using UnityEngine;

public class material_osset_moove : MonoBehaviour {
    [SerializeField] float scrollSpeed_x = 0.12F;
    [SerializeField] float scrollSpeed_y = 0.12F;
    [SerializeField] Renderer rend;

    void Start() {
        rend = GetComponent<Renderer>();
    }
    void Update() {
        float offset_x = Time.time * scrollSpeed_x;
        float offset_y = Time.time * scrollSpeed_y;
        rend.material.SetTextureOffset("_MainTex", new Vector2(offset_x, offset_y));
    }
}
