﻿using UnityEngine;

public class gk_camera_follow : MonoBehaviour {
    GameObject player; // player GO
    [SerializeField] Vector3 offset; // Camera Offset

    // Use this for initialization
    void Start() {
        player = GameObject.FindWithTag("Player"); // Find our player by tag
    }

    void LateUpdate() {
        transform.position = player.transform.position + offset; // Apply position with offset
    }

}