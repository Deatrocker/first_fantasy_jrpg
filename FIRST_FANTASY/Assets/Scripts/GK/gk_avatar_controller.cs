﻿using UnityEngine;
// https://www.patreon.com/deatrocker

public class gk_avatar_controller : MonoBehaviour {
    [SerializeField] float movement_speed = 1f; // movement speed as it is
    [SerializeField] AnimationClip idle; // idle animation clip
    [SerializeField] AnimationClip run; // run animation clip
    Animation animation_component; // dont mind
    Rigidbody rigidbody_component; // dont mind
    Vector3 moveDirection; // dont mind
    [SerializeField] bool can_control; // can player controll avatar (DO NOT MODIFY DIRECT! Use Enable_Controll/Disable_Controll methods)

    void Start() {
        animation_component = GetComponent<Animation>(); // get link to component automatically and just ones per launch
        rigidbody_component = GetComponent<Rigidbody>(); // get link to component automatically and just ones per launch
        if(!DATA_MANAGER.Get_Loaded_Status()) {
            Initialize();
        } else {
            DATA_MANAGER.Set_Loaded_Status(false);
        }
    }

    void Initialize() {
        transform.position = DATA_MANAGER.Get_Next_Scene_Position();
        transform.rotation = Quaternion.Euler(DATA_MANAGER.Get_Next_Scene_Rotation());
    }

    void FixedUpdate() {
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")); // its needed only cos quaternion sending trilliard infos when its zero
        if(moveDirection != Vector3.zero && can_control) { // if something pressed & can control
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDirection), 999f); // set avatar rotation
            rigidbody_component.MovePosition(transform.position + (transform.forward * movement_speed) * Time.deltaTime); // simply move avatar
            animation_component.clip = run; // set run animation to play
        } else {
            animation_component.clip = idle; // set idle animation to play
        }
        animation_component.Play(animation_component.clip.name); // play current animation clip
    }

    public void Enable_Controll() {
        if(can_control) {
            Debug.Log("Controll already enabled");
            return;
        }
        can_control = true;
    }

    public void Disable_Controll() {
        if(!can_control) {
            Debug.Log("Controll already disabled");
            return;
        }
        can_control = false;
    }


}