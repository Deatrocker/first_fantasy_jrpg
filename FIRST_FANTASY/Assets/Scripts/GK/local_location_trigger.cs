﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class local_location_trigger : MonoBehaviour {
    [SerializeField] string info = "Some location name"; // Просто название для идентификации сцены
    [SerializeField] ushort moove_to; // Название сцены, которую нужно загрузить

    void OnTriggerEnter(Collider other) {
        if(other.tag == "Player"){
            Debug.Log(string.Format("Trigger hit"));
            if(AsyncOperation.Equals(SceneManager.LoadSceneAsync(moove_to), true)) SceneManager.LoadScene(moove_to); // Загрузка уровня
        }
    }

}
