﻿using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Developer: Dmitry Deatrocker Raskalov https://vk.com/deatrocker and https://connect.unity.com/u/59e1bcd0880c64001985c55d (2018)
/// Script reacts when player hit the trigger to set next scene and player position and rotation in next scene
/// </summary>

public class gk_llk_trigger : MonoBehaviour {
    [SerializeField] string info = "Some location name"; // Just scene name for info (no sense to code)
    [SerializeField] ushort moove_to; // Scene number (buildindex)
    [SerializeField] Vector3 next_position; // Position in next scene for character
    [SerializeField] Vector3 next_rotation; // Rotation in next scene for character

    void OnTriggerEnter(Collider other) {
        if(other.tag == "Player"){ // If character hit the trigger
            DATA_MANAGER.Set_Net_Scene_Location(next_position, next_rotation); // Set position and rotation in next scene
            //if(AsyncOperation.Equals(SceneManager.LoadSceneAsync(moove_to), true)) // this is commented for now
            SceneManager.LoadScene(moove_to); // Level loading with selected buildindex
        }
    }

}